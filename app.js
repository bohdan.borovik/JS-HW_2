const students = [{name: 'Vasya', mark: 3.8, email: 'vasya@gmail.com', isAdmin: false},
    {name: 'Helen', mark: 3.4, email: 'helen@gmail.com', isAdmin: false},
    {name: 'Marina', mark: 4.0, email: 'marina@gmail.com', isAdmin: true},
    {name: 'Alex', mark: 4.2, email: 'alex@gmail.com', isAdmin: false},
    {name: 'Martin', mark: 4, email: 'martin@gmail.com', isAdmin: true},
    {name: 'Denys', mark: 3.7, email: 'denys@gmail.com', isAdmin: false},
    {name: 'Daniel', mark: 4.8, email: 'daniel@gmail.com', isAdmin: true},
    {name: 'John', mark: 3.6, email: 'jogn@gmail.com', isAdmin: false},
    {name: 'Phil', mark: 4.5, email: 'phil@gmail.com', isAdmin: false},
    {name: 'Anna', mark: 3.8, email: 'anna@gmail.com', isAdmin: false}];

console.log(students);

function logAdmins(students) {
    return students.filter(el => el.isAdmin === true)
    //вернуть из функции список тех, у кого поле isAdmin: true
}
let admin = logAdmins(students);
console.log(admin);

function filterByMark(students) {
    let inputMark = prompt('Input mark')
    return students.filter(el => el.mark >= inputMark);
    // return students.filter(function(student) {
    //     if(student.mark >= inputMark) {
    //         return student;
    //     }
    // });
    
    //ввести оценку через prompt
    //и вернуть из функции список тех, у кого в поле mark оценка >= чем введенная ранее
}
let mark = filterByMark(students);
console.log(mark);

function renderStudents(students) {
    let index = 0;
    let htmlRender = '';
    for(let item in students) {
        let student = students[item];
        index = +item++;
        htmlRender += `<tr>
        <td>${index}</td>
        <td>${student.name}</td>
        <td>${student.mark}</td>
        <td>${student.email}</td>
        <td>${student.isAdmin}</td>
    </tr>`;
    }
    document.querySelector('tbody').innerHTML = htmlRender;
    //по аналогии с курсами валют
}
let render = renderStudents(students);
console.log(render);

function getAverageMark(students) {
    let averageMark = students.reduce(function(acc, student) {
        return acc + student.mark;
    }, 0) / students.length;
    return averageMark.toFixed(2);
    //вернуть среднюю оценку (2 знака после запятой) из списка students
}
let average = getAverageMark(students);
console.log(average);

function getEmailList(students) {
    return students.map(el => el.email);
    //вернуть список состоящий из только поля email студентов
    // пример результата: ['vasya@gmail.com', 'helen@gmail.com', ..., 'anna@gmail.com']
}
let emailList = getEmailList(students);
console.log(emailList);

function getDataList(students) {
    return students.map(function(student) {
        return {name: student.name, email: student.email};
    });
    //вернуть список, состоящий из name и email студентов
    //пример результата: [{name: 'Vasya', email: 'vasya@gmail.com'}, {name: 'Helen', email: 'helen@gmail.com'}, ..., {name: 'Anna', email: 'anna@gmail.com'}]
}
let dataList = getDataList(students);
console.log(dataList);